//
//  SceneProtocols.swift
//  Galaga
//
//  Created by Javi Manzano on 28/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import SpriteKit

protocol SceneCollisionsProtocol: class {
    func checkCollisions ()
}
