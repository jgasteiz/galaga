//
//  GameScene.swift
//  Galaga
//
//  Created by Javi Manzano on 27/08/2016.
//  Copyright (c) 2016 Javi Manzano. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var playableRect: CGRect?
    
    var scrollingBackground: ScrollingBackground?
    var ship: Ship?
    
    var enemies: [Enemy] = []
    
    // Times for performing updates
    var lastUpdateTime: NSTimeInterval = 0
    var dt: NSTimeInterval = 0
    
    override init(size: CGSize) {
        super.init(size: size)
        
        ship = Ship(scene: self)
        
        // Spawn an enemy every 2 seconds
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock({
                    self.enemies.append(Enemy(scene: self))
                }),
                SKAction.waitForDuration(1.0)
            ])
        ))
        
        // Add a red rectangle to the scene to limit the playable boundaries.
        playableRect = Utils.getPlayableRect(scene: self)
        //if let playableRect = playableRect {
        //    addChild(Utils.getPlayableRectShape(playableRect))
        //}
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        // Set background
        scrollingBackground = ScrollingBackground(
            scene: self,
            bgImageName: "space",
            scrollingSpeed: 2.0)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let touchLocation = touch.locationInNode(self)
        
        if touchLocation.x < self.frame.size.width / 2 {
            ship?.moveLeft()
        } else {
            ship?.moveRight()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        ship?.stopMoving()
    }

    override func update(currentTime: CFTimeInterval) {
        if lastUpdateTime > 0 {
            dt = currentTime - lastUpdateTime
        } else {
            dt = 0
        }
        lastUpdateTime = currentTime
        
        scrollingBackground?.performUpdateOperations()
        
        ship?.performUpdateOperations(dt, currentTime: currentTime)
        for enemy in enemies {
            if enemy.isAlive {
                enemy.performUpdateOperations(dt, currentTime: currentTime)
            }
        }
    }
    
    override func didEvaluateActions() {
        checkCollisions()
    }
}

extension GameScene: SceneCollisionsProtocol {
    func checkCollisions () {
        var enemiesHitByShip: [Enemy] = []
        var enemiesHitByLaser: [Enemy] = []
        
        enumerateChildNodesWithName("enemy", usingBlock: { node, _ in
            let enemy = node as! Enemy
            
            self.enumerateChildNodesWithName("ship_laser", usingBlock: { node, _ in
                let laser = node as! SKShapeNode
                
                if CGRectIntersectsRect(enemy.frame, laser.frame) {
                    enemiesHitByLaser.append(enemy)
                    laser.removeFromParent()
                }
            })
            
            if let ship = self.ship {
                if enemy.isAlive && CGRectIntersectsRect(enemy.frame, ship.frame) {
                    enemiesHitByShip.append(enemy)
                }
            }
        })
        
        for enemy in enemiesHitByLaser {
            laserHitEnemy(enemy)
        }
        
        for enemy in enemiesHitByShip {
            enemyHitShip(enemy)
        }
    }
    
    func laserHitEnemy (enemy: Enemy) {
        enemy.explodeAndDie()
    }
    
    func enemyHitShip (enemy: Enemy) {
        enemy.explodeAndDie()
//        ship?.removeFromParent()
    }
}
