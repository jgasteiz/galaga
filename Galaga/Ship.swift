//
//  Ship.swift
//  Galaga
//
//  Created by Javi Manzano on 27/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import SpriteKit

class Ship: GameCharacter {
    
    var lasers: [SKShapeNode] = []
    
    init (scene: SKScene?) {
        super.init(textureName: "ship", scene: scene)
        
        characterSpeed = 580.0
        name = "ship"
        
        if let activeScene = activeScene {
            position = CGPoint(x: activeScene.size.width / 2 - size.width / 2, y: 60)
        }
        
        // Shoot laser every 0.5 seconds
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(shootLaser),
                SKAction.waitForDuration(0.5)
            ])
        ))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func shootLaser () {
        guard let activeScene = activeScene else {
            print("A scene must be defined to shoot a laser")
            return
        }
        
        let laserWidth: Double = 6.0
        let laserHeight: Double = 30.0
        
        let shape = SKShapeNode()
        let path = CGPathCreateMutable()
        
        let laserSpawnPoint = topCenterPosition
        let rect = CGRect(
            x: Double(laserSpawnPoint.x) - laserWidth / 2,
            y: Double(laserSpawnPoint.y) + laserHeight,
            width: laserWidth,
            height: laserHeight
        )
        CGPathAddRect(path, nil, rect)
        shape.path = path
        shape.name = "ship_laser"
        shape.fillColor = SKColor.redColor()
        shape.strokeColor = SKColor.redColor()
        
        // Add the laser to the scene
        activeScene.addChild(shape)
        lasers.append(shape)
        
        let actionMove = SKAction.moveToY(activeScene.size.height, duration: 2.0)
        let actionRemove = SKAction.removeFromParent()
        let removeLaserFromList = SKAction.runBlock({
            self.lasers.removeFirst()
        })
        
        shape.runAction(SKAction.sequence([actionMove, actionRemove, removeLaserFromList]))
    }
}

extension Ship: GameCharacterProtocol {
    func performUpdateOperations(dt: CFTimeInterval, currentTime: CFTimeInterval?) {
        if activeMovement == Movement.Still {
            return
        }
        
        // Set the movement destination (left/right) depeneding on its
        // current activeMovement.
        var direction = CGPoint(x: 0, y: 0)
        if activeMovement == Movement.Left {
            direction.x = -1
        } else if activeMovement == Movement.Right {
            direction.x = 1
        }
        
        // Only moving in the x axis
        let velocity = CGPoint(x: direction.x * characterSpeed, y: 0)
        let amountToMove = CGPoint(x: velocity.x * CGFloat(dt), y: velocity.y * CGFloat(dt))
        let newPosition = CGPoint(x: position.x + amountToMove.x,
                                  y: position.y + amountToMove.y)
        
        
        if isPositionWithinPlayableBounds(newPosition) {
            position = newPosition
        }
    }
}

