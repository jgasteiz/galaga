//
//  Enemy.swift
//  Galaga
//
//  Created by Javi Manzano on 27/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import SpriteKit

class Enemy: GameCharacter {
    
    var explosionAnimation: SKAction = SKAction()
    let yRadius: CGFloat = 10
    var xRadius: CGFloat = 10
    
    init (scene: SKScene?) {
        super.init(textureName: "enemy1", scene: scene)
        
        // Set the character properties
        characterSpeed = 380.0
        name = "enemy"
        
        // Randomize the radius a bit
        xRadius = CGFloat.random(min: xRadius, max: xRadius * 2.5)
        
        // Initialize the explosion animation
        var textures: [SKTexture] = []
        for i in 1...5 {
            textures.append(SKTexture(imageNamed: "enemy\(i)"))
        }
        explosionAnimation = SKAction.animateWithTextures(textures, timePerFrame: 0.1)
        
        // Set the initial position
        if let activeScene = activeScene {
            let playableRect = Utils.getPlayableRect(scene: activeScene)
            let randomX = CGFloat.random(
                min: playableRect.minX + xRadius,
                max: playableRect.maxX - size.width - xRadius * 2)
            position = CGPoint(x: randomX, y: activeScene.size.height)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func explodeAndDie () {
        if !isAlive {
            return
        }
        
        isAlive = false
        runAction(
            SKAction.sequence([
                explosionAnimation,
                SKAction.runBlock(removeFromParent)
            ])
        )
    }
}

extension Enemy: GameCharacterProtocol {
    func performUpdateOperations(dt: CFTimeInterval, currentTime: CFTimeInterval?) {
        
        // Only moving in the y axis (for now)
        let velocity = CGPoint(x: 0, y: -characterSpeed)
        let amountToMove = CGPoint(x: velocity.x * CGFloat(dt), y: velocity.y * CGFloat(dt))
        
        let speed: Double = 400
        let degrees = Double(currentTime! * speed) % 360.0
        let radians: CGFloat = CGFloat(degrees * M_PI / 180.0)
        
        let newPosition = CGPoint(
            x: position.x + cos(radians) * xRadius,
            y: position.y + sin(radians) * yRadius + amountToMove.y)
        
        print(newPosition)
        
        position = newPosition
        
        if newPosition.y < 0 {
            removeFromParent()
            isAlive = false
        }
    }
}


