//
//  ScrollingBackground.swift
//  Galaga
//
//  Created by Javi Manzano on 27/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import SpriteKit

class ScrollingBackground {
    
    let bg1: SKSpriteNode
    let bg2: SKSpriteNode
    let scrollingSpeed: CGFloat
    
    init (scene: SKScene, bgImageName: String, scrollingSpeed: CGFloat) {
        bg1 = SKSpriteNode(imageNamed: bgImageName)
        bg2 = SKSpriteNode(imageNamed: bgImageName)
        self.scrollingSpeed = scrollingSpeed
        
        bg1.position = CGPoint(x: 0, y: 0)
        bg1.anchorPoint = CGPointZero
        bg1.zPosition = -1
        scene.addChild(bg1)
        
        bg2.position = CGPoint(x: 0, y: bg1.size.height)
        bg2.anchorPoint = CGPointZero
        bg2.zPosition = -1
        scene.addChild(bg2)
    }
    
    func performUpdateOperations() {
        bg1.position = CGPoint(x: bg1.position.x, y: bg1.position.y - scrollingSpeed)
        bg2.position = CGPoint(x: bg2.position.x, y: bg2.position.y - scrollingSpeed)
        
        if bg1.position.y < -bg1.size.height {
            bg1.position = CGPoint(x: bg1.position.x, y: bg2.position.y + bg2.size.height)
        }
        
        if bg2.position.y < -bg2.size.height {
            bg2.position = CGPoint(x: bg2.position.x, y: bg1.position.y + bg1.size.height)
        }
    }
}