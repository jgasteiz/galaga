//
//  GameCharacter.swift
//  Galaga
//
//  Created by Javi Manzano on 27/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import SpriteKit

protocol GameCharacterProtocol: class {
    func performUpdateOperations (dt: CFTimeInterval, currentTime: CFTimeInterval?)
}

class GameCharacter: SKSpriteNode {
    var characterSpeed: CGFloat = 0
    
    var isAlive = true
    
    let activeScene: SKScene?
    
    var _activeMovement: Movement = Movement.Still
    
    init(textureName: String, scene: SKScene?) {
        activeScene = scene
        
        let texture = SKTexture(imageNamed: textureName)
        super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
        anchorPoint = CGPointZero
        
        // Add itself to the scene, if there's any.
        activeScene?.addChild(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func moveLeft() {
        _activeMovement = Movement.Left
    }
    
    func moveRight() {
        _activeMovement = Movement.Right
    }
    
    func stopMoving() {
        _activeMovement = Movement.Still
    }
    
    func isPositionWithinPlayableBounds (position: CGPoint) -> Bool {
        guard let activeScene = activeScene else {
            return false
        }
        
        let playableRect = Utils.getPlayableRect(scene: activeScene)
        
        return Utils.isPointWithinBounds(point: position, rect: playableRect, size: size)
    }
    
    var topCenterPosition: CGPoint {
        return CGPoint(
            x: self.position.x + self.size.width / 2,
            y: self.position.y + self.size.height)
    }
    
    var activeMovement: Movement {
        return _activeMovement
    }
    
}
